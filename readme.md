# Solidity version
pragma solidity >=0.4.17 < 0.7.0;

Solidity Doc
https://solidity.readthedocs.io/en/v0.6.12/

Solidity coding for smart contract
http://remix.ethereum.org/

To get ethers
https://faucet.rinkeby.io/
https://faucet.metamask.io/

Crypto wallet
https://metamask.io/

Host in ropsten network
https://faucet.ropsten.be/

smart contract address: 0xdd6570dc8d632ebb79c297bbd1eb69786cd1b0f9
