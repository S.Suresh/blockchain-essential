pragma solidity >=0.4.17 < 0.7.0;

contract carMaster {
    string OwnerOfCar;
    
    function carMaster(string newOwner) public {
        OwnerOfCar = newOwner;
    }
    
    function setDetails(string newOwner) public {
        OwnerOfCar = newOwner;
    }
    
    function getDetail() public view returns(string){
        return(OwnerOfCar);
    }
}